<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\CommentStoreRequest;
use App\Http\Requests\Comments\CommentUpdateRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(CommentStoreRequest $request)
    {
        $comment = Comment::create($request->validated());
        return new CommentResource($comment);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            $comment->with(['user', 'article']);
            return new CommentResource($comment);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CommentUpdateRequest $request, string $id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            $data = $request->validated();
            $comment->update($data);
            return new CommentResource($comment);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            $comment->delete();
            return response()->json([
                'error' => false,
                'code'  => 200,
                'message' => 'Image was deleted!'
            ], 200);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }
}
