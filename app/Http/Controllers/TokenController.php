<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function store(Request $request) {
        $token_name = $request->input('token_name');
        $token = $request->user()->createToken($token_name);

        return response()->json([
            'token' => $token->plainTextToken
        ]);
    }
}
